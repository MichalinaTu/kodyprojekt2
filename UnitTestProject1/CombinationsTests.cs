﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;
using System.Collections.Generic;

namespace UnitTestProject1
{
    [TestClass]
    public class CombinationsTests
    {
        [TestMethod]
        public void Coefficient_3_2()
        {
            //when
            ulong coef = Combination.BinomialCoefficient(3, 2);
            //then
            Assert.AreEqual((ulong)3, coef);
        }

        [TestMethod]
        public void Coefficient_4_2()
        {
            //when
            ulong coef = Combination.BinomialCoefficient(4, 2);
            //then
            Assert.AreEqual((ulong)6, coef);
        }

        [TestMethod]
        public void Coefficient_32_3()
        {
            //when
            ulong coef = Combination.BinomialCoefficient(32, 3);
            //then
            Assert.AreEqual((ulong)4960, coef);
        }

        [TestMethod]
        public void Combinations_3_2()
        {
            //given
            int n = 3;
            int k = 2;
            //when
            Dictionary<ulong, int[]> combs = Combination.Combinations(n, k);
            //then
            Assert.AreEqual(0, combs[0][0]);
            Assert.AreEqual(1, combs[0][1]);
            Assert.AreEqual(0, combs[1][0]);
            Assert.AreEqual(2, combs[1][1]);
            Assert.AreEqual(1, combs[2][0]);
            Assert.AreEqual(2, combs[2][1]);
        }

        [TestMethod]
        public void Combinations_15_1()
        {
            //given
            int n = 15;
            int k = 1;
            //when
            Dictionary<ulong, int[]> combs = Combination.Combinations(n, k);
            //then
            for(int i = 0; i < n; i++)
            {
                Assert.AreEqual(i, combs[(ulong)i][0]);
            }
        }
    }
}

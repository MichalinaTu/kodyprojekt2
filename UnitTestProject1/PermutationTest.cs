﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class PermutationTest
    {
        [TestMethod]
        public void InverseTest2()
        {
            //given
            PermutationMatrix matrix = new PermutationMatrix(2);
            //when
            PermutationMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(2,2), mul);
        }

        [TestMethod]
        public void InverseTest5()
        {
            //given
            PermutationMatrix matrix = new PermutationMatrix(5);
            //when
            PermutationMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(5, 2), mul);
        }

        [TestMethod]
        public void InverseTest64()
        {
            //given
            PermutationMatrix matrix = new PermutationMatrix(64);
            //when
            PermutationMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(64, 2), mul);
        }
    }
}

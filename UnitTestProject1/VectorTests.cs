﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class VectorTests
    {
        [TestMethod]
        public void RandomVectorTest()
        {
            //given
            int d = 8;
            int w = 7;
            //when
            Vector vector = Vector.GenerateRandomBinaryVector(d, w);
            int sum = 0;
            for (int i = 0; i < d; i++)
            {
                sum += vector[i];
            }
            //then
            Assert.AreEqual(w, sum);
        }

        [TestMethod]
        public void EqualTest()
        {
            //given
            Vector v1 = new Vector(new int[] { 0, 1, 0, 1, 0, 1 }, 2);
            int[] tab2 = { 0, 1, 0, 1, 0, 1 };
            //when
            Vector v2 = new Vector(tab2, 2);
            Vector v3 = Vector.GenerateRandomBinaryVector(6, 4);
            //then
            Assert.AreEqual(v1, v2);
            Assert.AreNotEqual(v1, v3);
        }

        [TestMethod]
        public void VectorToStringTest()
        {
            //given
            Vector v1 = new Vector(new int[] { 0, 1, 0, 1, 0, 1 }, 2);
            //when
            string s1 = v1.ToString();
            string s2 = "|0, 1, 0, 1, 0, 1|";
            //then
            Assert.AreEqual(s1, s2);
        }

        [TestMethod]
        public void VectorMultiplyMatrixTest()
        {
            //given
            Vector v1 = new Vector(new int[] { 0, 1, 1, 1 }, 2);
            MatrixMod m2 = new MatrixMod(new int[,] { { 0, 1 }, { 1, 0 }, { 1, 1 }, { 0, 0 } }, 2);
            //when
            Vector vec = v1 * m2;
            //then
            Vector expectedVec = new Vector(new int[] { 0, 1 }, 2);
            Assert.AreEqual(expectedVec, vec);
        }

        [TestMethod]
        public void MatrixMultiplyVectorTest()
        {
            //given        
            MatrixMod m1 = new MatrixMod(new int[,] { { 0, 1 }, 
                                                      { 1, 0 }, 
                                                      { 1, 1 }, 
                                                      { 0, 0 } }, 2);
            Vector v2 = new Vector(new int[] { 0, 1 }, 2);
            //when
            Vector vec = m1 * v2;
            //then
            Vector expectedVec = new Vector(new int[] { 1,0, 1,0 }, 2);
            Assert.AreEqual(expectedVec, vec);
        }

    }
}

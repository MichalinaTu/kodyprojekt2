﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class InvertibleMatrixTests
    {
        [TestMethod]
        public void InverseTest2()
        {
            //given
            int n = 2;
            InvertibleMatrix matrix = new InvertibleMatrix(n);
            //when
            InvertibleMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(n, 2), mul);
        }

        [TestMethod]
        public void InverseTest8()
        {
            //given
            int n = 8;
            InvertibleMatrix matrix = new InvertibleMatrix(n);
            //when
            InvertibleMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(n, 2), mul);
        }

        [TestMethod]
        public void InverseTest124()
        {
            //given
            int n = 124;
            InvertibleMatrix matrix = new InvertibleMatrix(n);
            //when
            InvertibleMatrix inverseMatrix = matrix.Inverse();
            MatrixMod mul = matrix * inverseMatrix;
            //then
            Assert.AreEqual(MatrixMod.GetEyeMatrix(n, 2), mul);
        }
    }
}

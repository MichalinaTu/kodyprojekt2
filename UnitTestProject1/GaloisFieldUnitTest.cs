using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class GaloisFieldUnitTest
    {
        [TestMethod]
        public void InverseTest()
        {
            //given
            GaloisField GF = new GaloisField(7);
            //when
            int[] inverse = new int[7];
            for(int i = 0; i < 7; i++)
            {
                inverse[i] = GF.Inverse(i);
            }
            //then
            int[] expectedInverse = { 0, 1, 4, 5, 2, 3, 6};
            for(int i = 0; i < 7; i++)
            {
                Assert.AreEqual(expectedInverse[i], inverse[i]);
            }
        }
    }
}

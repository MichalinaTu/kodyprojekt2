﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class GoppaTests
    {
        [TestMethod]
        public void FindErrorTest64()
        {
            //given
            Vector message = Vector.GenerateRandomBinaryVector(34, 15);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n64_k34_t5);
            Vector error = Vector.GenerateRandomBinaryVector(64, 5);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector messageWithError = incomingMessage + error;
            Vector foundError = goppaCode.FindError(messageWithError);
            Vector fixedVector = goppaCode.FixedMessage(messageWithError);
            //then
            Assert.AreEqual(error, foundError);
            Assert.AreEqual(incomingMessage, fixedVector);
        }

        [TestMethod]
        public void DecodeTest64()
        {
            //given
            Vector message = Vector.GenerateRandomBinaryVector(34, 15);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n64_k34_t5);
            Vector error = Vector.GenerateRandomBinaryVector(64, 5);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector messageWithError = incomingMessage + error;
            Vector decodedMessage = goppaCode.Deencrypt(messageWithError);
            //then
            Assert.AreEqual(message, decodedMessage);
        }

        [TestMethod]
        public void DecodeForElieceTest64()
        {
            //given
            Vector message = Vector.GenerateRandomBinaryVector(34, 15);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n64_k34_t5);
            Vector error = Vector.GenerateRandomBinaryVector(64, 5);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector messageWithError = incomingMessage + error;
            Vector decodedMessage = goppaCode.Deencrypt(messageWithError);
            //then
            Assert.AreEqual(message, decodedMessage);
        }

        [TestMethod]
        public void FindErrorTest32()
        {
            //given
            Vector message = new Vector(new int[] { 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0 }, 2);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n32_k17_t3);
            Vector error = Vector.GenerateRandomBinaryVector(32, 3);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector messageWithError = incomingMessage + error;
            Vector foundError = goppaCode.FindError(messageWithError);
            Vector fixedVector = goppaCode.FixedMessage(messageWithError);
            //then
            Assert.AreEqual(error, foundError);
            Assert.AreEqual(incomingMessage, fixedVector);
        }

        [TestMethod]
        public void DecodeTest32()
        {
            //given
            Vector message = new Vector(new int[] { 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0 }, 2);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n32_k17_t3);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector decodedMessage = goppaCode.Deencrypt(incomingMessage);
            //then
            Assert.AreEqual(message, decodedMessage);
        }

        [TestMethod]
        public void DecodeForElieceTest32()
        {
            //given
            Vector message = new Vector(new int[] { 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0 }, 2);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.n32_k17_t3);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector decodedMessage = goppaCode.DeencryptForMcEliece(incomingMessage);
            //then
            Assert.AreEqual(message, decodedMessage);
        }

        [TestMethod]
        public void FindErrorTest0()
        {
            //given
            Vector message = new Vector(new int[] { 1 }, 2);
            GoppaCode goppaCode = new GoppaCode(GoppaSize.test);
            Vector error = new Vector(new int[] { 0,0,0,1,1 }, 2);
            //when
            Vector incomingMessage = goppaCode.Encrypt(message);
            Vector messageWithError = incomingMessage + error;
            Vector foundError = goppaCode.FindError(messageWithError);
            Vector fixedVector = goppaCode.FixedMessage(messageWithError);
            //then
            Assert.AreEqual(error, foundError);
            Assert.AreEqual(incomingMessage, fixedVector);
        }
    }
}

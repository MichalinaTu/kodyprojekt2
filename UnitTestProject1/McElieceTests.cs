﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class McElieceTests
    {
        [TestMethod]
        public void DeEncryptTest32()
        {
            //given
            Vector message = new Vector(new int[] { 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0 }, 2);
            McElieceCode mcElieceCode = new McElieceCode(GoppaSize.n32_k17_t3);
            //when
            Vector incomingMessage = mcElieceCode.Encrypt(message);
            Vector decodedMessage = mcElieceCode.Decrypt(incomingMessage);
            //then
            Assert.AreEqual(message, decodedMessage);
        }

        [TestMethod]
        public void DeEncryptTest64()
        {
            //given
            Vector message = Vector.GenerateRandomBinaryVector(34, 15);
            McElieceCode mcElieceCode = new McElieceCode(GoppaSize.n64_k34_t5);
            //when
            Vector incomingMessage = mcElieceCode.Encrypt(message);
            Vector decryptedMessage = mcElieceCode.Decrypt(incomingMessage);
            //then
            Assert.AreEqual(message, decryptedMessage);
        }

        //[TestMethod]
        //public void DeEncryptTest128()
        //{
        //    //given
        //    Vector message = Vector.GenerateRandomBinaryVector(65, 30);
        //    McElieceCode mcElieceCode = new McElieceCode(GoppaSize.n128_k65_t9);
        //    //when
        //    Vector incomingMessage = mcElieceCode.Encrypt(message);
        //    Vector decryptedMessage = mcElieceCode.Decrypt(incomingMessage);
        //    //then
        //    Assert.AreEqual(message, decryptedMessage);
        //}
    }
}

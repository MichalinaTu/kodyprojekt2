﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

using ProjektKody2;

namespace UnitTestProject1
{
    [TestClass]
    public class MatrixModTests
    {
        [TestMethod]
        public void StringTest()
        {
            //given
            MatrixMod matrix = new MatrixMod(2, 3, 8);
            //when
            string matrixString = matrix.ToString();
            //then
            string expectedString = "|0, 0, 0|\n|0, 0, 0|\n";
            Assert.AreEqual(expectedString, matrixString);
        }

        [TestMethod]
        public void AddTest()
        {
            //given
            int[,] m1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
            int[,] m2 = { { 1, 1 }, { 2, 2 }, { 1, 1 } };
            MatrixMod matrix1 = new MatrixMod(m1, 6);
            MatrixMod matrix2 = new MatrixMod(m2, 6);
            //when
            MatrixMod sumMatrix = matrix1+matrix2;
            //then
            MatrixMod expectedMatrix = new MatrixMod(new int[,] { { 2, 3 }, { 5, 0 }, { 0, 1 } },6);
            Assert.AreEqual(expectedMatrix, sumMatrix);
        }

        [TestMethod]
        public void SubTest()
        {
            //given
            int[,] m1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
            int[,] m2 = { { 2, 2 }, { 2, 2 }, { 1, 1 } };
            MatrixMod matrix1 = new MatrixMod(m1, 6);
            MatrixMod matrix2 = new MatrixMod(m2, 6);
            //when
            MatrixMod subMatrix = matrix1 - matrix2;
            //then
            MatrixMod expectedMatrix = new MatrixMod(new int[,] { { 5, 0 }, { 1, 2 }, { 4, 5 } }, 6);
            Assert.AreEqual(expectedMatrix, subMatrix);
        }

        [TestMethod]
        public void MulTest()
        {
            //given
            int[,] m1 = { { 1, 2 }, { 3, 4 }, { 5, 6 } };
            int[,] m2 = { { 1, 1, 1 }, { 2, 1, 3 } };
            MatrixMod matrix1 = new MatrixMod(m1, 6);
            MatrixMod matrix2 = new MatrixMod(m2, 6);
            //when
            MatrixMod mulMatrix = matrix1 * matrix2;
            //then
            MatrixMod expectedMatrix = new MatrixMod(new int[,] { { 5, 3, 1 }, { 5, 1, 3 }, { 5, 5, 5 } }, 6);
            Assert.AreEqual(expectedMatrix, mulMatrix);
        }

        [TestMethod]
        public void MulTest2()
        {
            //given
            int[,] m1 = { { 1, 0, 1, 1 }, 
                          { 0, 1, 0, 1 },
                          { 1, 1, 0, 1 },
                          { 1, 1, 1, 1 } };
            int[,] m2 = { { 0, 1, 1, 0 },
                          { 1, 0, 0, 1 },
                          { 0, 0, 1, 1 },
                          { 1, 1, 0, 1 } };
            MatrixMod matrix1 = new MatrixMod(m1, 2);
            MatrixMod matrix2 = new MatrixMod(m2, 2);
            //when
            MatrixMod mulMatrix = matrix1 * matrix2;
            //then
            MatrixMod expectedMatrix = MatrixMod.GetEyeMatrix(4,2);
            Assert.AreEqual(expectedMatrix, mulMatrix);
        }


    }
}

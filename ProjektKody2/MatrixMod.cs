﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class MatrixMod
    {
        protected GaloisField GF;
        protected int d1;
        protected int d2;

        protected int[,] matrix;

        public int q { get { return GF.q; } }
        public int Dimension(int i)
        {
            if (i == 0) return d1;
            if (i == 1) return d2;
            throw new IndexOutOfRangeException();           
        }
        public int this[int i, int j] { get { return matrix[i, j]; } }

        public MatrixMod() { }

        public MatrixMod(int k, int n, int q)
        {
            d1 = k;
            d2 = n;
            GF = new GaloisField(q);
            matrix = new int[d1, d2];
        }

        public MatrixMod(int k, int n, GaloisField GaloisF)
        {
            d1 = k;
            d2 = n;
            GF = GaloisF;
            matrix = new int[d1, d2];
        }

        public MatrixMod(int[,] matrix, int q)
        {
            d1 = matrix.GetLength(0);
            d2 = matrix.GetLength(1);
            GF = new GaloisField(q);
            this.matrix = new int[d1, d2];
            for(int i=0;i<d1;i++)
                for(int j = 0; j < d2; j++)
                {
                    this.matrix[i, j] = GF.GetMod(matrix[i, j]);
                }
        }

        public MatrixMod(int[,] matrix, GaloisField GaloisF)
        {
            d1 = matrix.GetLength(0);
            d2 = matrix.GetLength(1);
            GF = GaloisF;
            this.matrix = new int[d1, d2];
            for (int i = 0; i < d1; i++)
                for (int j = 0; j < d2; j++)
                {
                    this.matrix[i, j] = GF.GetMod(matrix[i, j]);
                }
        }

        static public MatrixMod GetEyeMatrix(int n, int q)
        {
            int[,] m = new int[n, n];
            for (int i = 0; i < n; i++)
                m[i, i] = 1;
            return new MatrixMod(m, q);
        }

        static public MatrixMod operator *(MatrixMod m, int s)
        {
            MatrixMod matrix = new MatrixMod(m.d1,m.d2,m.GF);
            for(int i = 0; i < m.d1; i++)
            {
                for (int j = 0; j < m.d2; j++)
                {
                    matrix.matrix[i, j] = m.GF.GetMod(m.matrix[i, j] * s);
                }
            }
            return matrix;
        }

        static public MatrixMod operator *(MatrixMod m1, MatrixMod m2)
        {
            if (m1.q != m2.q)
                throw new ArgumentException("Nie można mnożyć macierzy nad różnymi ciałami");
            if (m1.d2 != m2.d1)
                throw new ArgumentException("Nie można mnożyć macierzy o niezgodnych wymiarach");
            MatrixMod matrix = new MatrixMod(m1.d1, m2.d2, m1.GF);
            for (int i = 0; i < m1.d1; i++)
            {
                for (int j = 0; j < m2.d2; j++)
                {
                    int sum = 0;
                    for (int k = 0; k < m1.d2; k++)
                    {
                        sum += m1.matrix[i, k] * m2.matrix[k, j];
                    }
                    matrix.matrix[i, j] = m1.GF.GetMod(sum);
                }
            }
            return matrix;
        }

        static public MatrixMod operator +(MatrixMod m1, MatrixMod m2)
        {
            if (m1.q != m2.q)
                throw new ArgumentException("Nie można dodać macierzy nad różnymi ciałami");
            if(m1.d1!=m2.d1 || m1.d2!=m2.d2)
                throw new ArgumentException("Nie można dodać macierzy o różnych wymiarach");
            MatrixMod matrix = new MatrixMod(m1.d1, m1.d2, m1.GF);
            for (int i = 0; i < m1.d1; i++)
            {
                for (int j = 0; j < m1.d2; j++)
                {
                    matrix.matrix[i, j] = m1.GF.GetMod(m1.matrix[i, j] + m2.matrix[i, j]);
                }
            }
            return matrix;
        }

        static public MatrixMod operator -(MatrixMod m1, MatrixMod m2)
        {
            if (m1.q != m2.q)
                throw new ArgumentException("Nie można dodać macierzy nad różnymi ciałami");
            if (m1.d1 != m2.d1 || m1.d2 != m2.d2)
                throw new ArgumentException("Nie można dodać macierzy o różnych wymiarach");
            MatrixMod matrix = new MatrixMod(m1.d1, m1.d2, m1.GF);
            for (int i = 0; i < m1.d1; i++)
            {
                for (int j = 0; j < m1.d2; j++)
                {
                    matrix.matrix[i, j] = m1.GF.GetMod(m1.matrix[i, j] - m2.matrix[i, j]);
                }
            }
            return matrix;
        }






        public override string ToString()
        {
            string[] rows = new string[d1];
            string[] cols = new string[d2+1];
            cols[0] = "|";
            for(int i = 0; i < d1; i++)
            {
                for (int j = 0; j < d2-1; j++)
                {
                    cols[j+1] = $"{matrix[i, j]}, ";
                }
                cols[d2] = $"{matrix[i, d2 - 1]}|\n";
                rows[i] = String.Concat(cols);
            }
            return String.Concat(rows);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is MatrixMod))
                return false;
            MatrixMod matrix = obj as MatrixMod;
            if (matrix.d1 != d1 || matrix.d2 != d2)
                return false;
            if (matrix.GF.q != GF.q)
                return false;
            for (int i = 0; i < d1; i++)
                for (int j = 0; j < d2; j++)
                    if (matrix.matrix[i, j] != this.matrix[i, j])
                        return false;
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class GaloisField
    {
        private int qMod;
        public int q { get { return qMod; } }

        private int[] inverse;

        public GaloisField (int q)
        {
            qMod = q;
            CalculateInverseElements();
        }

        public int GetMod(int x)
        {
            x = x % qMod;
            if (x < 0) x += qMod;
            return x;
        }

        public int Inverse (int i)
        {
            if (i >= qMod)
                throw new IndexOutOfRangeException();
            return inverse[i];
        }

        private void CalculateInverseElements()
        {
            inverse = new int[q + 1];
            for (int i = 1; i <= q; i++)
            {
                if (inverse[i] == 0)
                {
                    for (int j = 1; j <= q; j++)
                    {
                        if (inverse[j] == 0)
                        {
                            if (i * j % q == 1)
                            {
                                inverse[i] = j;
                                inverse[j] = i;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
}

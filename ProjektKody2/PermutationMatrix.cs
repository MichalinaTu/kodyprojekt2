﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class PermutationMatrix : MatrixMod
    {
        public PermutationMatrix (int[] permutation)
        {
            d1 = d2 = permutation.Length;
            matrix = new int[d1, d2];
            this.GF = new GaloisField(2);
            for (int i = 0; i < d1; i++)
                matrix[i, permutation[i]] = 1;
        }

        public PermutationMatrix (int n)
        {
            d1 = d2 = n;
            matrix = new int[n, n];
            GF = new GaloisField(2);
            int[] permutation = GenerateRandomPermutation();
            for (int i = 0; i < d1; i++)
                matrix[i, permutation[i]] = 1;
        }

        private int[] GenerateRandomPermutation()
        {
            Random rand = new Random();
            int[] permutation = new int[d1];
            for(int i = 0; i < d1; i++)
            {
                permutation[i] = i;
                int j = rand.Next(i + 1);
                permutation[i] = permutation[j];
                permutation[j] = i;
            }
            return permutation;
        }

        public PermutationMatrix Inverse()
        {
            int[] permutation = new int[d1];
            for (int i = 0; i < d1; i++)
                for(int j = 0; j < d2; j++)
                    if (this[j, i] == 1)
                    {
                        permutation[i] = j;
                        break;
                    }
            return new PermutationMatrix(permutation);
        }
    }
}

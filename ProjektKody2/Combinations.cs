﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public static class Combination
    {
        public static ulong BinomialCoefficient (int n, int k)
        {
            ulong coef = 1;
            for(ulong i = 1; i <= (ulong)k; i++)
            {
                coef = coef * ((ulong)n - i + 1) / i;
            }
            return coef;
        }

        public static Dictionary<ulong, int[]> Combinations(int n, int k)
        {
            ulong coef = BinomialCoefficient(n, k);
            Dictionary<ulong, int[]> combs = new Dictionary<ulong, int[]>();
            //int[][] combs = new int[coef][];
            for(ulong i = 0; i < coef; i++)
            {
                combs[i] = new int[k];
            }
            Combinations_rec(n, k, ref combs, 0, 0, -1);
            return combs;
        }

        private static void Combinations_rec(int n, int k, ref Dictionary<ulong, int[]> combs, ulong pocz, int place, int last)
        {
            ulong p = pocz;
            ulong coef0;
            for(int i = last + 1; i <= n - (k - place); i++)
            {
                coef0 = BinomialCoefficient(n - (i + 1), k - place - 1);
                for (ulong j = 0; j < coef0; j++)
                    combs[p + j][place] = i;             
                if (place + 1 < k)
                    Combinations_rec(n, k, ref combs, p, place + 1, i);
                p += coef0;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class InvertibleMatrix : MatrixMod
    {
        public InvertibleMatrix(int n)
        {
            d1 = d2 = n;
            matrix = new int[n, n];
            GF = new GaloisField(2);
            for (int i = 0; i < d1; i++)
            {
                matrix[i, i] = 1;
            }
        }


        public static InvertibleMatrix RandomInvertibleMatrix(int n)
        {
            InvertibleMatrix randomMatrix = new InvertibleMatrix(n);
            Random rand = new Random();
            for(int i = 0; i < randomMatrix.d1; i++)
            {
                
                int j = rand.Next(randomMatrix.d1);
                while (j == i)
                    j = rand.Next(randomMatrix.d1);
                for (int k = 0; k < randomMatrix.d2; k++)
                    randomMatrix.matrix[i, k] = randomMatrix.GF.GetMod(randomMatrix.matrix[j, k]+ randomMatrix.matrix[i,k]);
            }
            return (randomMatrix);
        }

        public InvertibleMatrix Inverse()
        {
            InvertibleMatrix copyMatrix = MakeCopy();
            InvertibleMatrix inverseMatrix = new InvertibleMatrix(d1);
            for(int i = 0; i < d1; i++)
            {
                if (copyMatrix[i, i] == 0)
                {
                    int j = i + 1;
                    while (j < d1)
                    {
                        if (copyMatrix[j, i] == 1)
                        {
                            copyMatrix.ChangeRows(i, j);
                            inverseMatrix.ChangeRows(i, j);
                            break;
                        }
                    }
                }

                for(int w = 0; w < i; w++)
                {
                    if (copyMatrix[w, i] == 1)
                    {
                        copyMatrix.AddRowToRow(w, i);
                        inverseMatrix.AddRowToRow(w, i);
                    }
                }
                for(int w = i + 1; w < d1; w++)
                {
                    if (copyMatrix[w, i] == 1)
                    {
                        copyMatrix.AddRowToRow(w, i);
                        inverseMatrix.AddRowToRow(w, i);
                    }
                }
                
            }
            return inverseMatrix;
        }

        private void ChangeRows(int r1, int r2)
        {
            int x;
            for(int i = 0; i < d1; i++)
            {
                x = matrix[r1, i];
                matrix[r1, i] = matrix[r2, i];
                matrix[r2, i] = x;
            }
        }

        private void AddRowToRow(int addTo, int addFrom)
        {
            for (int i = 0; i < d1; i++)
            {
                if (matrix[addTo, i] == 0)
                    matrix[addTo, i] = matrix[addFrom, i];
                else
                    if (matrix[addFrom, i] == 1)
                    matrix[addTo, i] = 0;
            }
        }

        private InvertibleMatrix MakeCopy()
        {
            InvertibleMatrix newMatrix = new InvertibleMatrix(d1);
            for(int i=0;i<d1;i++)
                for(int j = 0; j < d1; j++)
                {
                    newMatrix.matrix[i, j] = matrix[i, j];
                }
            return newMatrix;
        }

    }
}

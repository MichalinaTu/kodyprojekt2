﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public static class BinaryHelper
    {
        /// <summary>
        /// Konwersja liczby z systemu 10(int) do systemu 2(int[n])
        /// </summary>
        public static int[] ToBinary(int a, int n)
        {
            int[] bin = new int[n];
            for (int i = 0; i < n; i++)
            {
                bin[n-1-i] = (a % 2);
                a /= 2;
            }
            return bin;
        }
        /// <summary>
        /// Konwersja tablicy z systemu 2(int[]) do systemu 10(int)
        /// </summary>
        public static int ToInt(int[] bin)
        {
            int a = 0;
            int p = 1;
            for (int i = 0; i < bin.Length; i++)
            {
                a += bin[bin.Length - i - 1] * p;
                p *= 2;
            }
            return a;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
   public class McElieceCode
    {
        public int k;
        public int n;
        private InvertibleMatrix S;
        private PermutationMatrix P;
        private GoppaCode Goppa;

        public MatrixMod SGP;
        public int t;

        public McElieceCode(GoppaSize size)
        {         
            Goppa = new GoppaCode(size);
            k = Goppa.k;
            n = Goppa.n;
            S = InvertibleMatrix.RandomInvertibleMatrix(k);
            P = new PermutationMatrix(n);

            SGP = S * Goppa.G * P;
            t = Goppa.t;
        }

        public Vector Encrypt (Vector m)
        {
            if (m.Dimension != k)
                throw new ArgumentException("Wiadomość złego wymiaru");
            Vector cp = m * SGP;
            Vector z = Vector.GenerateRandomBinaryVector(n, t);
            return (cp + z);
        }

        public Vector Decrypt(Vector c)
        {
            Vector cp = c * P.Inverse();
            Vector mp = Goppa.DeencryptForMcEliece(cp);
            return (mp * S.Inverse());
        }
    }
}

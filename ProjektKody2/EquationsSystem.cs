﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class EquationsSystem
    {
        public int[][] matrix;

        public int[] b;

        private int n1;
        private int n2;

        public EquationsSystem() { }

        public EquationsSystem(Vector b, MatrixMod G)
        {
            n2 = G.Dimension(0);
            n1 = G.Dimension(1);
            if (b.Dimension != n1)
                throw new ArgumentException("Niezgodne wymiary macierzy i wektora");
            this.b = CopyMod(b);
            
            this.matrix = CopyMod(G);
        }

        public Vector Resolve()
        {
            Gauss();
            return new Vector(b, n2, 2);
        }


        public void Gauss()
        {
            for (int i = 0; i < n2; i++)
            {
                if (matrix[i][i] == 0)
                {
                    int j = i + 1;
                    while (j < n1)
                    {
                        if (matrix[j][i] == 1)
                        {
                            int x = b[j];
                            b[j] = b[i];
                            b[i] = x;
                            int[] xx = matrix[j];
                            matrix[j] = matrix[i];
                            matrix[i] = xx;
                            break;
                        }
                    }
                }
                MakeZeros(i);
            }
        }

        private void AddRowToRow(int addTo, int addFrom)
        {
            if(matrix[addTo][addFrom]==1)
            for (int i = 0; i < n2; i++)
            {
                if (matrix[addTo][i] == 0)
                    matrix[addTo][i] = matrix[addFrom][i];
                else
                    if (matrix[addFrom][ i] == 1)
                    matrix[addTo][i] = 0;
            }
        }


        public void MakeZeros(int i)
        {
            for (int l = 0; l < i; l++)
            {
                AddRowToRow(l, i);
            }
            for (int l = i + 1; l < n1; l++)
            {
                AddRowToRow(l, i);
            }
        }


        private int[] CopyMod(Vector t)
        {
            int[] copy = new int[t.Dimension];
            for (int i = 0; i < t.Dimension; i++)
            {
                copy[i] = t[i];
            }
            return copy;
        }

        private int[][] CopyMod(MatrixMod g)
        {
            int[][] copy = new int[g.Dimension(1)][];
            for (int j=0;j<g.Dimension(1);j++)
            {
                copy[j] = new int[g.Dimension(0)];
                for(int i = 0; i < g.Dimension(0); i++)
                    copy[j][i] = g[i,j];
            }
            return copy;
        }
    }
}

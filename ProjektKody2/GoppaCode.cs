﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class GoppaCode
    {
        public MatrixMod G;
        public MatrixMod H;
        public int n;
        public int k;
        public int t;

        public GoppaCode(GoppaSize size)
        {
            t = GoppaCodesMatrixes.t[(int)size];
            n = GoppaCodesMatrixes.n[(int)size];
            k = GoppaCodesMatrixes.k[(int)size];

            G = GoppaCodesMatrixes.G[(int)size];
            H = GoppaCodesMatrixes.H[(int)size];

        }

        public Vector Encrypt(Vector m)
        {
            return m*G;
        }

        public Vector Deencrypt(Vector u)
        {
            Vector up = FixedMessage(u);
            EquationsSystem equations = new EquationsSystem(up, G);
            return equations.Resolve();
        }

        public Vector DeencryptForMcEliece(Vector u)
        {
            Vector up = u + FindErrorMaxWeight(u);
            EquationsSystem equations = new EquationsSystem(up, G);
            return equations.Resolve();
        }

        public Vector FixedMessage(Vector u)
        {
            return u + FindError(u);
        }

        public Vector FindError(Vector u)
        {
            Vector synd = H * u;
            if (synd.IsZero())
                return new Vector(n, 2);
            for (int i = 1; i <= t; i++)
            {
                Dictionary<ulong, int[]> combins = Combination.Combinations(H.Dimension(1), i);
                ulong coefficient = Combination.BinomialCoefficient(H.Dimension(1), i);
                for(ulong c = 0; c < coefficient; c++)
                {
                    int[] combin = combins[c];
                    int k;
                    for(k = 0; k < synd.Dimension; k++)
                    {
                        int sum = 0;
                        for (int j = 0; j < combin.Length; j++)
                            sum += H[k, combin[j]];
                        if (sum % 2 != synd[k])
                            break;
                    }
                    if (k == synd.Dimension)
                        return Vector.GetBinaryVector(H.Dimension(1), combin);
                }
            }
            return null;
        }

        public Vector FindErrorMaxWeight(Vector u)
        {
            Vector synd = H * u;
            Dictionary<ulong, int[]> combins = Combination.Combinations(H.Dimension(1), t);
            ulong coefficient = Combination.BinomialCoefficient(H.Dimension(1), t);
            for (ulong c = 0; c < coefficient; c++)
            {
                int[] combin = combins[c];
                int k;
                for (k = 0; k < synd.Dimension; k++)
                {
                    int sum = 0;
                    for (int j = 0; j < combin.Length; j++)
                        sum += H[k, combin[j]];
                    if (sum % 2 != synd[k])
                        break;
                }
                if (k == synd.Dimension)
                    return Vector.GetBinaryVector(H.Dimension(1), combin);
            }
            return new Vector(n, 2);
        }
    }
}

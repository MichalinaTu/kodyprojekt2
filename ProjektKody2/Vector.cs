﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjektKody2
{
    public class Vector
    {
        int[] tab;
        int d;
        GaloisField GF;

        public int this[int i] { get { return tab[i]; } }
        public int Dimension { get { return d; } }
        public int q { get { return GF.q; } }
        
        public int[] GetTab()
        {
            return tab;
        }

        public Vector(int[] tab, int q)
        {
            GF = new GaloisField(q);
            d = tab.Length;
            this.tab = tab;
        }

        public Vector(int[] tab, GaloisField GF)
        {
            this.GF = GF;
            d = tab.Length;
            this.tab = tab;
        }

        public Vector(int[] tab, int n, int q)
        {
            GF = new GaloisField(q);
            d = n;
            this.tab = new int[d];
            for (int i = 0; i < d; i++)
                this.tab[i] = tab[i];
        }

        public Vector(int n, int q)
        {
            GF = new GaloisField(q);
            d = n;
            this.tab = new int[d];
        }

        public Vector(int n, GaloisField GF)
        {
            this.GF = GF;
            d = n;
            this.tab = new int[d];
        }

        public bool IsZero()
        {
            for(int i = 0; i < d; i++)
            {
                if (tab[i] != 0)
                    return false;
            }
            return true;
        }

        static public Vector GenerateRandomBinaryVector (int dimension, int weight)
        {
            if (weight > dimension)
                throw new ArgumentException("Nie można stworzyć wektora o wadze większej niż wymiar.");
            Vector vector = new Vector(dimension, 2);
            Random random = new Random();
            while (weight > 0)
            {
                int i = random.Next(dimension);
                if (vector[i] == 1)
                    continue;
                weight--;
                vector.tab[i] = 1;
                if (dimension == i - 1)
                    dimension--;
            }
            return vector;
        }

        static public Vector GetBinaryVector(int dimension, int[] ones)
        {
            Vector vector = new Vector(dimension, 2);
            for (int i = 0; i < ones.Length; i++)
                vector.tab[ones[i]] = 1;
            return vector;
        }

        static public Vector operator +(Vector v1, Vector v2)
        {
            if (v1.q != v2.q)
                throw new ArgumentException("Nie można dodać wektorów nad różnymi ciałami");
            if (v1.d != v2.d || v1.d != v2.d)
                throw new ArgumentException("Nie można dodać wektorów o różnych wymiarach");
            Vector vector = new Vector(v1.d, v1.GF);
            for (int i = 0; i < v1.d; i++)
                vector.tab[i] = v1.GF.GetMod(v1[i] + v2[i]);
            return vector;
        }

        static public Vector operator *(MatrixMod m1, Vector v2)
        {
            if (m1.q != v2.q)
                throw new ArgumentException("Nie można mnożyć obiektów nad różnymi ciałami");
            if (m1.Dimension(1) != v2.d)
                throw new ArgumentException("Nie można mnożyć obiektów o niezgodnych wymiarach");
            Vector vector = new Vector(m1.Dimension(0), v2.GF);
            for (int i = 0; i < m1.Dimension(0); i++)
            {
                    int sum = 0;
                    for (int k = 0; k < m1.Dimension(1); k++)
                    {
                        sum += m1[i, k] * v2[k];
                    }
                vector.tab[i] = vector.GF.GetMod(sum);
                
            }
            return vector;
        }

        static public Vector operator *(Vector v1, MatrixMod m2)
        {
            if (v1.q != m2.q)
                throw new ArgumentException("Nie można mnożyć obiektów nad różnymi ciałami");
            if (v1.d != m2.Dimension(0))
                throw new ArgumentException("Nie można mnożyć obiektów o niezgodnych wymiarach");
            Vector vector = new Vector(m2.Dimension(1), v1.GF);
            for (int i = 0; i < m2.Dimension(1); i++)
            {
                int sum = 0;
                for (int k = 0; k < m2.Dimension(0); k++)
                {
                    sum += v1[k] * m2[k, i];
                }
                vector.tab[i] = vector.GF.GetMod(sum);

            }
            return vector;
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Vector))
                return false;
            Vector vector = obj as Vector;
            if (vector.d != d)
                return false;
            if (vector.GF.q != GF.q)
                return false;
            for (int i = 0; i < d; i++)
                    if (vector[i] != this[i])
                        return false;
            return true;
        }

        public override string ToString()
        {
            string[] cols = new string[d + 1];
            cols[0] = "|";
            for (int i = 0; i < d; i++)
            {
                cols[i + 1] = $"{tab[i]}, ";
            }
            cols[d] = $"{tab[d - 1]}|";
            return String.Concat(cols);
        }
    }
}

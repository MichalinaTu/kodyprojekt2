﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjektKody2
{
    public partial class Form1 : Form
    {
        McElieceCode elieceCode;

        GoppaSize size = GoppaSize.n32_k17_t3;

        public Form1()
        {
            InitializeComponent();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton32.Checked)
                size = GoppaSize.n32_k17_t3;
            else if (radioButton64.Checked)
                size = GoppaSize.n64_k34_t5;
            else
                size = GoppaSize.n128_k65_t9;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (elieceCode == null)
                return;
            int k = elieceCode.k;
            string text = richTextBoxMessage.Text;
            int[][] sequences = StringHelper.GetBinarySequences(text, k);
            int[][] encrypted = new int[sequences.Length][];
            for(int i = 0; i < sequences.Length; i++)
            {
                encrypted[i] = elieceCode.Encrypt(new Vector(sequences[i], 2)).GetTab();
            }
            string encryptedText = StringHelper.GetText(encrypted);
            richTextBoxEncrypted.Text = encryptedText;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            elieceCode = new McElieceCode(size);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (elieceCode == null)
                return;
            int n = elieceCode.n;
            string text = richTextBoxEncrypted.Text;
            int[][] sequences = StringHelper.GetBinarySequences(text, n);
            int[][] decrypted = new int[sequences.Length][];
            for (int i = 0; i < sequences.Length; i++)
            {
                decrypted[i] = elieceCode.Decrypt(new Vector(sequences[i], 2)).GetTab();
            }
            string decryptedText = StringHelper.GetText(decrypted);
            richTextBoxDecrypted.Text = decryptedText;
        }
    }
}
